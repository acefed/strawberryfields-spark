package io.gitlab.acefed.strawberryfieldsspark;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static spark.Spark.*;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.github.cdimascio.dotenv.Dotenv;

public class StrawberryfieldsSparkApplication {
    public static Dotenv dotenv = Dotenv.configure().ignoreIfMissing().load();
    public static ObjectMapper objectMapper = new ObjectMapper();
    public static String configJson;
    public static JsonNode CONFIG;
    public static String ME;
    public static RSAPrivateCrtKey PRIVATE_KEY;
    public static PublicKey PUBLIC_KEY;
    public static String publicKeyPem;

    public static RSAPrivateCrtKey importPrivateKey(String pem) throws Exception {
        String header = "-----BEGIN PRIVATE KEY-----";
        String footer = "-----END PRIVATE KEY-----";
        String b64 = pem;
        b64 = b64.replace("\\n", "");
        b64 = b64.replace("\n", "");
        if (b64.startsWith("\"")) b64 = b64.substring(1);
        if (b64.endsWith("\"")) b64 = b64.substring(0, b64.length() - 1);
        if (b64.startsWith(header)) b64 = b64.substring(header.length());
        if (b64.endsWith(footer)) b64 = b64.substring(0, b64.length() - footer.length());
        KeyFactory kf = KeyFactory.getInstance("RSA");
        byte[] der = Base64.getDecoder().decode(b64);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(der);
        RSAPrivateCrtKey key = (RSAPrivateCrtKey) kf.generatePrivate(spec);
        return key;
    }

    public static PublicKey privateKeyToPublicKey(RSAPrivateCrtKey privateKey) throws Exception {
        KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec spec = new RSAPublicKeySpec(privateKey.getModulus(), privateKey.getPublicExponent());
        PublicKey publicKey = kf.generatePublic(spec);
        return publicKey;
    }

    public static String exportPublicKey(PublicKey key) {
        byte[] der = key.getEncoded();
        String b64 = Base64.getEncoder().encodeToString(der);
        String pem = "-----BEGIN PUBLIC KEY-----" + "\n";
        while (b64.length() > 64) {
            pem += b64.substring(0, 64) + "\n";
            b64 = b64.substring(64);
        }
        pem += b64.substring(0, b64.length()) + "\n";
        pem += "-----END PUBLIC KEY-----" + "\n";
        return pem;
    }

    public static String uuidv7() {
        SecureRandom rand = new SecureRandom();
        byte[] v = new byte[16];
        rand.nextBytes(v);
        long ts = System.currentTimeMillis();
        v[0] = (byte) (ts >> 40);
        v[1] = (byte) (ts >> 32);
        v[2] = (byte) (ts >> 24);
        v[3] = (byte) (ts >> 16);
        v[4] = (byte) (ts >> 8);
        v[5] = (byte) ts;
        v[6] = (byte) (v[6] & 0x0F | 0x70);
        v[8] = (byte) (v[8] & 0x3F | 0x80);
        StringBuilder sb = new StringBuilder(32);
        for (byte b : v) sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public static String talkScript(String req) {
        long ts = System.currentTimeMillis();
        if (URI.create(req).getHost().equals("localhost")) return "<p>" + ts + "</p>";
        
        List<String> script = new ArrayList<>();
        script.add("<p>");
        script.add("<a href=\"https://");
        script.add(URI.create(req).getHost());
        script.add("/\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">");
        script.add(URI.create(req).getHost());
        script.add("</a>");
        script.add("</p>");

        return String.join("", script);
    }

    public static String getActivity(String username, String hostname, String req) throws Exception {
        String t = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH)
            .withZone(ZoneId.of("GMT"))
            .format(Instant.now());

        List<String> message = new ArrayList<>();
        message.add("(request-target): get " + URI.create(req).getPath());
        message.add("host: " + URI.create(req).getHost());
        message.add("date: " + t);

        Signature sig = Signature.getInstance("SHA256withRSA");
        sig.initSign(PRIVATE_KEY);
        sig.update(String.join("\n", message).getBytes("UTF-8"));
        String b64 = Base64.getEncoder().encodeToString(sig.sign());

        Map<String, String> headers = new HashMap<>();
        List<String> signature = new ArrayList<>();
        headers.put("Date", t);
        signature.add("keyId=\"https://" + hostname + "/u/" + username + "#Key\"");
        signature.add("algorithm=\"rsa-sha256\"");
        signature.add("headers=\"(request-target) host date\"");
        signature.add("signature=\"" + b64 + "\"");
        headers.put("Signature", String.join(",", signature));
        headers.put("Accept", "application/activity+json");
        headers.put("Accept-Encoding", "identity");
        headers.put("Cache-Control", "no-cache");
        headers.put("User-Agent", "StrawberryFields-Spark/2.0.0 (+https://" + hostname + "/)");

        OkHttpClient httpClient = new OkHttpClient();
        Request client = new Request.Builder()
            .url(req)
            .headers(Headers.of(headers))
            .build();
        Response res = httpClient.newCall(client).execute();
        String status = Integer.toString(res.code());
        System.out.println("GET " + req + " " + status);
        return res.body().string();
    }

    public static void postActivity(
        String username,
        String hostname,
        String req,
        Map<String, Object> x
    ) throws Exception {
        String t = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH)
            .withZone(ZoneId.of("GMT"))
            .format(Instant.now());
        String body = objectMapper.writeValueAsString(x);
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(body.getBytes("UTF-8"));
        String s256 = Base64.getEncoder().encodeToString(digest.digest());

        List<String> message = new ArrayList<>();
        message.add("(request-target): post " + URI.create(req).getPath());
        message.add("host: " + URI.create(req).getHost());
        message.add("date: " + t);
        message.add("digest: SHA-256=" + s256);

        Signature sig = Signature.getInstance("SHA256withRSA");
        sig.initSign(PRIVATE_KEY);
        sig.update(String.join("\n", message).getBytes("UTF-8"));
        String b64 = Base64.getEncoder().encodeToString(sig.sign());

        Map<String, String> headers = new HashMap<>();
        List<String> signature = new ArrayList<>();
        headers.put("Date", t);
        headers.put("Digest", "SHA-256=" + s256);
        signature.add("keyId=\"https://" + hostname + "/u/" + username + "#Key\"");
        signature.add("algorithm=\"rsa-sha256\"");
        signature.add("headers=\"(request-target) host date digest\"");
        signature.add("signature=\"" + b64 + "\"");
        headers.put("Signature", String.join(",", signature));
        headers.put("Accept", "application/json");
        headers.put("Accept-Encoding", "gzip");
        headers.put("Cache-Control", "max-age=0");
        headers.put("User-Agent", "StrawberryFields-Spark/2.0.0 (+https://" + hostname + "/)");

        System.out.println("POST " + req + " " + body);
        OkHttpClient httpClient = new OkHttpClient();
        Request client = new Request.Builder()
            .url(req)
            .post(RequestBody.create(body, MediaType.parse("application/activity+json")))
            .headers(Headers.of(headers))
            .build();
        httpClient.newCall(client).execute();
    }

    public static void acceptFollow(String username, String hostname, JsonNode x, JsonNode y) throws Exception {
        String aid = uuidv7();

        Map<String, Object> body = new HashMap<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid);
        body.put("type", "Accept");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("object", y);

        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void follow(String username, String hostname, JsonNode x) throws Exception {
        String aid = uuidv7();

        Map<String, Object> body = new HashMap<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid);
        body.put("type", "Follow");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("object", x.get("id").textValue());

        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void undoFollow(String username, String hostname, JsonNode x) throws Exception {
        String aid = uuidv7();

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> objectMap = new HashMap<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "#Undo");
        body.put("type", "Undo");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("object", objectMap);
        objectMap.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("type", "Follow");
        objectMap.put("actor", "https://" + hostname + "/u/" + username);
        objectMap.put("object", x.get("id").textValue());

        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void like(String username, String hostname, JsonNode x, JsonNode y) throws Exception {
        String aid = uuidv7();

        Map<String, Object> body = new HashMap<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid);
        body.put("type", "Like");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("object", x.get("id").textValue());

        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void undoLike(String username, String hostname, JsonNode x, JsonNode y) throws Exception {
        String aid = uuidv7();

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> objectMap = new HashMap<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "#Undo");
        body.put("type", "Undo");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("object", objectMap);
        objectMap.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("type", "Like");
        objectMap.put("actor", "https://" + hostname + "/u/" + username);
        objectMap.put("object", x.get("id").textValue());

        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void announce(String username, String hostname, JsonNode x, JsonNode y) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();

        Map<String, Object> body = new HashMap<>();
        List<String> toList = new ArrayList<>();
        List<String> ccList = new ArrayList<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity");
        body.put("type", "Announce");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("published", t);
        body.put("to", toList);
        toList.add("https://www.w3.org/ns/activitystreams#Public");
        body.put("cc", ccList);
        ccList.add("https://" + hostname + "/u/" + username + "/followers");
        body.put("object", x.get("id").textValue());

        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void undoAnnounce(
        String username,
        String hostname,
        JsonNode x,
        JsonNode y,
        String z
    ) throws Exception {
        String aid = uuidv7();

        Map<String, Object> body = new HashMap<>();
        Map<String, Object> objectMap = new HashMap<>();
        List<String> toList = new ArrayList<>();
        List<String> ccList = new ArrayList<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "#Undo");
        body.put("type", "Undo");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("object", objectMap);
        objectMap.put("id", z + "/activity");
        objectMap.put("type", "Announce");
        objectMap.put("actor", "https://" + hostname + "/u/" + username);
        objectMap.put("to", toList);
        toList.add("https://www.w3.org/ns/activitystreams#Public");
        objectMap.put("cc", ccList);
        ccList.add("https://" + hostname + "/u/" + username + "/followers");
        objectMap.put("object", x.get("id").textValue());

        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void createNote(String username, String hostname, JsonNode x, String y) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();

        Map<String, Object> body = new HashMap<>();
        List<String> toList = new ArrayList<>();
        List<String> ccList = new ArrayList<>();
        Map<String, Object> objectMap = new HashMap<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity");
        body.put("type", "Create");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("published", t);
        body.put("to", toList);
        toList.add("https://www.w3.org/ns/activitystreams#Public");
        body.put("cc", ccList);
        ccList.add("https://" + hostname + "/u/" + username + "/followers");
        body.put("object", objectMap);
        objectMap.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("type", "Note");
        objectMap.put("attributedTo", "https://" + hostname + "/u/" + username);
        objectMap.put("content", talkScript(y));
        objectMap.put("url", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("published", t);
        objectMap.put("to", toList);
        objectMap.put("cc", ccList);

        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void createNoteImage(
        String username,
        String hostname,
        JsonNode x,
        String y,
        String z
    ) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();

        Map<String, Object> body = new HashMap<>();
        List<String> toList = new ArrayList<>();
        List<String> ccList = new ArrayList<>();
        Map<String, Object> objectMap = new HashMap<>();
        List<Map<String, String>> attachmentList = new ArrayList<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity");
        body.put("type", "Create");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("published", t);
        body.put("to", toList);
        toList.add("https://www.w3.org/ns/activitystreams#Public");
        body.put("cc", ccList);
        ccList.add("https://" + hostname + "/u/" + username + "/followers");
        body.put("object", objectMap);
        objectMap.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("type", "Note");
        objectMap.put("attributedTo", "https://" + hostname + "/u/" + username);
        objectMap.put("content", talkScript("https://localhost"));
        objectMap.put("url", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("published", t);
        objectMap.put("to", toList);
        objectMap.put("cc", ccList);
        objectMap.put("attachment", attachmentList);
        attachmentList.add(new HashMap<>());
        attachmentList.get(0).put("type", "Image");
        attachmentList.get(0).put("mediaType", z);
        attachmentList.get(0).put("url", y);

        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void createNoteMention(
        String username,
        String hostname,
        JsonNode x,
        JsonNode y,
        String z
    ) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
        String at =
            "@" + y.get("preferredUsername").textValue() + "@" + URI.create(y.get("inbox").textValue()).getHost();

        Map<String, Object> body = new HashMap<>();
        List<String> toList = new ArrayList<>();
        List<String> ccList = new ArrayList<>();
        Map<String, Object> objectMap = new HashMap<>();
        List<Map<String, String>> tagList = new ArrayList<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity");
        body.put("type", "Create");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("published", t);
        body.put("to", toList);
        toList.add("https://www.w3.org/ns/activitystreams#Public");
        body.put("cc", ccList);
        ccList.add("https://" + hostname + "/u/" + username + "/followers");
        body.put("object", objectMap);
        objectMap.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("type", "Note");
        objectMap.put("attributedTo", "https://" + hostname + "/u/" + username);
        objectMap.put("inReplyTo", x.get("id").textValue());
        objectMap.put("content", talkScript(z));
        objectMap.put("url", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("published", t);
        objectMap.put("to", toList);
        objectMap.put("cc", ccList);
        objectMap.put("tag", tagList);
        tagList.add(new HashMap<>());
        tagList.get(0).put("type", "Mention");
        tagList.get(0).put("name", at);

        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void createNoteHashtag(
        String username,
        String hostname,
        JsonNode x,
        String y,
        String z
    ) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();

        Map<String, Object> body = new HashMap<>();
        List<Object> contextList = new ArrayList<>();
        Map<String, String> contextListMap = new HashMap<>();
        List<String> toList = new ArrayList<>();
        List<String> ccList = new ArrayList<>();
        Map<String, Object> objectMap = new HashMap<>();
        List<Map<String, String>> tagList = new ArrayList<>();
        body.put("@context", contextList);
        contextList.add("https://www.w3.org/ns/activitystreams");
        contextList.add(contextListMap);
        contextListMap.put("Hashtag", "as:Hashtag");
        body.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity");
        body.put("type", "Create");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("published", t);
        body.put("to", toList);
        toList.add("https://www.w3.org/ns/activitystreams#Public");
        body.put("cc", ccList);
        ccList.add("https://" + hostname + "/u/" + username + "/followers");
        body.put("object", objectMap);
        objectMap.put("id", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("type", "Note");
        objectMap.put("attributedTo", "https://" + hostname + "/u/" + username);
        objectMap.put("content", talkScript(y));
        objectMap.put("url", "https://" + hostname + "/u/" + username + "/s/" + aid);
        objectMap.put("published", t);
        objectMap.put("to", toList);
        objectMap.put("cc", ccList);
        objectMap.put("tag", tagList);
        tagList.add(new HashMap<>());
        tagList.get(0).put("type", "Hashtag");
        tagList.get(0).put("name", "#" + z );

        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void updateNote(String username, String hostname, JsonNode x, String y) throws Exception {
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
        long ts = System.currentTimeMillis();

        Map<String, Object> body = new HashMap<>();
        List<String> toList = new ArrayList<>();
        List<String> ccList = new ArrayList<>();
        Map<String, Object> objectMap = new HashMap<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", y + "#" + ts);
        body.put("type", "Update");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("published", t);
        body.put("to", toList);
        toList.add("https://www.w3.org/ns/activitystreams#Public");
        body.put("cc", ccList);
        ccList.add("https://" + hostname + "/u/" + username + "/followers");
        body.put("object", objectMap);
        objectMap.put("id", y);
        objectMap.put("type", "Note");
        objectMap.put("attributedTo", "https://" + hostname + "/u/" + username);
        objectMap.put("content", talkScript("https://localhost"));
        objectMap.put("url", y);
        objectMap.put("updated", t);
        objectMap.put("to", toList);
        objectMap.put("cc", ccList);

        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void deleteTombstone(String username, String hostname, JsonNode x, String y) throws Exception {
        Map<String, Object> body = new HashMap<>();
        Map<String, Object> objectMap = new HashMap<>();
        body.put("@context", "https://www.w3.org/ns/activitystreams");
        body.put("id", y + "#Delete");
        body.put("type", "Delete");
        body.put("actor", "https://" + hostname + "/u/" + username);
        body.put("object", objectMap);
        objectMap.put("id", y);
        objectMap.put("type", "Tombstone");

        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void main(String[] args) throws Exception {
        if (dotenv.get("CONFIG_JSON") != null && !dotenv.get("CONFIG_JSON").isEmpty()) {
            configJson = dotenv.get("CONFIG_JSON");
            if (configJson.startsWith("'")) configJson = configJson.substring(1);
            if (configJson.endsWith("'")) {
                configJson = configJson.substring(0, configJson.length() - 1);
            }
        } else configJson = String.join("\n", Files.readAllLines(Paths.get("data/config.json")));
        CONFIG = objectMapper.readTree(configJson);

        List<String> mine = new ArrayList<>();
        mine.add("<a href=\"https://");
        mine.add(URI.create(CONFIG.get("actor").get(0).get("me").textValue()).getHost());
        mine.add("/\" rel=\"me nofollow noopener noreferrer\" target=\"_blank\">");
        mine.add("https://");
        mine.add(URI.create(CONFIG.get("actor").get(0).get("me").textValue()).getHost());
        mine.add("/");
        mine.add("</a>");

        ME = String.join("", mine);
        PRIVATE_KEY = importPrivateKey(dotenv.get("PRIVATE_KEY"));
        PUBLIC_KEY = privateKeyToPublicKey(PRIVATE_KEY);
        publicKeyPem = exportPublicKey(PUBLIC_KEY);

        staticFiles.location("/public");
        notFound("<h1>Not Found</h1>");
        internalServerError("<h1>internal Server Error</h1>");
        port(Integer.parseInt(dotenv.get("PORT", "8080")));

        get("/", (req, res) -> {
            res.type("text/plain");
            return "StrawberryFields Spark";
        });

        get("/about", (req, res) -> {
            res.type("text/plain");
            return "About: Blank";
        });

        get("/u/:username", (req, res) -> {
            String username = req.params("username");
            String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
            String acceptHeaderField = req.headers("Accept");
            Boolean hasType = false;
            if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) return null;
            if (acceptHeaderField.contains("application/activity+json")) hasType = true;
            if (acceptHeaderField.contains("application/ld+json")) hasType = true;
            if (acceptHeaderField.contains("application/json")) hasType = true;
            if (!hasType) {
                String body = username + ": " + CONFIG.get("actor").get(0).get("name").textValue();

                Map<String, String> headers = new HashMap<>();
                headers.put("Cache-Control", "public, max-age=" + CONFIG.get("ttl").asText() + ", must-revalidate");
                headers.put("Vary", "Accept, Accept-Encoding");
                headers.forEach(res::header);

                res.type("text/plain");
                return body;
            }

            Map<String, Object> body = new HashMap<>();
            List<Object> contextList = new ArrayList<>();
            Map<String, String> contextListMap = new HashMap<>();
            Map<String, String> endpointsMap = new HashMap<>();
            List<Map<String, String>> attachmentList = new ArrayList<>();
            Map<String, String> iconMap = new HashMap<>();
            Map<String, String> imageMap = new HashMap<>();
            Map<String, String> publicKeyMap = new HashMap<>();
            body.put("@context", contextList);
            contextList.add("https://www.w3.org/ns/activitystreams");
            contextList.add("https://w3id.org/security/v1");
            contextList.add(contextListMap);
            contextListMap.put("schema", "https://schema.org/");
            contextListMap.put("PropertyValue", "schema:PropertyValue");
            contextListMap.put("value", "schema:value");
            contextListMap.put("Key", "sec:Key");
            body.put("id", "https://" + hostname + "/u/" + username);
            body.put("type", "Person");
            body.put("inbox", "https://" + hostname + "/u/" + username + "/inbox");
            body.put("outbox", "https://" + hostname + "/u/" + username + "/outbox");
            body.put("following", "https://" + hostname + "/u/" + username + "/following");
            body.put("followers", "https://" + hostname + "/u/" + username + "/followers");
            body.put("preferredUsername", username);
            body.put("name", CONFIG.get("actor").get(0).get("name").textValue());
            body.put("summary", "<p>2.0.0</p>");
            body.put("url", "https://" + hostname + "/u/" + username);
            body.put("endpoints", endpointsMap);
            endpointsMap.put("sharedInbox", "https://" + hostname + "/u/" + username + "/inbox");
            body.put("attachment", attachmentList);
            attachmentList.add(new HashMap<>());
            attachmentList.get(0).put("type", "PropertyValue");
            attachmentList.get(0).put("name", "me");
            attachmentList.get(0).put("value", ME);
            body.put("icon", iconMap);
            iconMap.put("type", "Image");
            iconMap.put("mediaType", "image/png");
            iconMap.put("url", "https://" + hostname + "/public/" + username + "u.png");
            body.put("image", imageMap);
            imageMap.put("type", "Image");
            imageMap.put("mediaType", "image/png");
            imageMap.put("url", "https://" + hostname + "/public/" + username + "s.png");
            body.put("publicKey", publicKeyMap);
            publicKeyMap.put("id", "https://" + hostname + "/u/" + username + "#Key");
            publicKeyMap.put("type", "Key");
            publicKeyMap.put("owner", "https://" + hostname + "/u/" + username);
            publicKeyMap.put("publicKeyPem", publicKeyPem);

            Map<String, String> headers = new HashMap<>();
            headers.put("Cache-Control", "public, max-age=" + CONFIG.get("ttl").asText() + ", must-revalidate");
            headers.put("Vary", "Accept, Accept-Encoding");
            headers.forEach(res::header);

            res.type("application/activity+json");
            return objectMapper.writeValueAsString(body);
        });

        get("/u/:username/inbox", (req, res) -> {
            res.status(405);
            return "";
        });
        post("/u/:username/inbox", (req, res) -> {
            String username = req.params("username");
            String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
            String contentTypeHeaderField = req.headers("Content-Type");
            Boolean hasType = false;
            String y = req.body();
            JsonNode ny = objectMapper.readTree(y);
            String t = Objects.toString(ny.path("type").textValue(), "");
            String aid = Objects.toString(ny.path("id").textValue(), "");
            String atype = Objects.toString(ny.path("type").textValue(), "");
            if (aid.length() > 1024 || atype.length() > 64) {
                res.status(400);
                return "";
            }
            System.out.println("INBOX " + aid + " " + atype);
            if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) return null;
            if (contentTypeHeaderField.contains("application/activity+json")) hasType = true;
            if (contentTypeHeaderField.contains("application/ld+json")) hasType = true;
            if (contentTypeHeaderField.contains("application/json")) hasType = true;
            if (!hasType) {
                res.status(400);
                return "";
            }
            if (req.headers("Digest") == null || req.headers("Digest").isEmpty()) {
                res.status(400);
                return "";
            }
            if (req.headers("Signature") == null || req.headers("Signature").isEmpty()) {
                res.status(400);
                return "";
            }
            if (t.equals("Accept") || t.equals("Reject") || t.equals("Add")) return "";
            if (t.equals("Remove") || t.equals("Like") || t.equals("Announce")) return "";
            if (t.equals("Create") || t.equals("Update") || t.equals("Delete")) return "";
            if (t.equals("Follow")) {
                if (!URI.create(Objects.toString(ny.path("actor").textValue(), "about:blank"))
                        .getScheme()
                        .equals("https")) {
                    res.status(400);
                    return "";
                }
                String x = getActivity(username, hostname, ny.get("actor").textValue());
                if (x == null || x.isEmpty()) {
                    res.status(500);
                    return "";
                }
                JsonNode nx = objectMapper.readTree(x);
                acceptFollow(username, hostname, nx, ny);
                return "";
            }
            if (t.equals("Undo")) {
                ObjectNode nz = ny.withObject("object");
                t = Objects.toString(nz.path("type").textValue(), "");
                if (t.equals("Accept") || t.equals("Like") || t.equals("Announce")) return "";
                if (t.equals("Follow")) {
                    if (!URI.create(Objects.toString(ny.path("actor").textValue(), "about:blank"))
                            .getScheme()
                            .equals("https")) {
                        res.status(400);
                        return "";
                    }
                    String x = getActivity(username, hostname, ny.get("actor").textValue());
                    if (x == null || x.isEmpty()) {
                        res.status(500);
                        return "";
                    }
                    JsonNode nx = objectMapper.readTree(x);
                    acceptFollow(username, hostname, nx, nz);
                    return "";
                }
            }
            res.status(500);
            return "";
        });

        post("/u/:username/outbox", (req, res) -> {
            res.status(405);
            return "";
        });
        get("/u/:username/outbox", (req, res) -> {
            String username = req.params("username");
            String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
            if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) return null;

            Map<String, Object> body = new HashMap<>();
            body.put("@context", "https://www.w3.org/ns/activitystreams");
            body.put("id", "https://" + hostname + "/u/" + username + "/outbox");
            body.put("type", "OrderedCollection");
            body.put("totalItems", 0);

            res.type("application/activity+json");
            return objectMapper.writeValueAsString(body);
        });

        get("/u/:username/following", (req, res) -> {
            String username = req.params("username");
            String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
            if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) return null;

            Map<String, Object> body = new HashMap<>();
            body.put("@context", "https://www.w3.org/ns/activitystreams");
            body.put("id", "https://" + hostname + "/u/" + username + "/following");
            body.put("type", "OrderedCollection");
            body.put("totalItems", 0);

            res.type("application/activity+json");
            return objectMapper.writeValueAsString(body);
        });

        get("/u/:username/followers", (req, res) -> {
            String username = req.params("username");
            String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
            if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) return null;

            Map<String, Object> body = new HashMap<>();
            body.put("@context", "https://www.w3.org/ns/activitystreams");
            body.put("id", "https://" + hostname + "/u/" + username + "/followers");
            body.put("type", "OrderedCollection");
            body.put("totalItems", 0);

            res.type("application/activity+json");
            return objectMapper.writeValueAsString(body);
        });

        post("/s/:secret/u/:username", (req, res) -> {
            String username = req.params("username");
            String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
            String body = req.body();
            JsonNode send = objectMapper.readTree(body);
            String t = Objects.toString(send.path("type").textValue(), "");
            if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) return null;
            if (req.params("secret") == null || req.params("secret").isEmpty()) return null;
            if (req.params("secret").equals("-")) return null;
            if (!req.params("secret").equals(dotenv.get("SECRET"))) return null;
            if (!URI.create(Objects.toString(send.path("id").textValue(), "about:blank")).getScheme().equals("https")) {
                res.status(400);
                return "";
            }
            String x = getActivity(username, hostname, Objects.toString(send.get("id").textValue(), ""));
            if (x == null || x.isEmpty()) {
                res.status(500);
                return "";
            }
            JsonNode nx = objectMapper.readTree(x);
            String aid = Objects.toString(nx.path("id").textValue(), "");
            String atype = Objects.toString(nx.path("type").textValue(), "");
            if (aid.length() > 1024 || atype.length() > 64) {
                res.status(400);
                return "";
            }
            if (t.equals("follow")) {
                follow(username, hostname, nx);
                return "";
            }
            if (t.equals("undo_follow")) {
                undoFollow(username, hostname, nx);
                return "";
            }
            if (t.equals("like")) {
                if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                        .getScheme()
                        .equals("https")) {
                    res.status(400);
                    return "";
                }
                String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
                if (y == null || y.isEmpty()) {
                    res.status(500);
                    return "";
                }
                JsonNode ny = objectMapper.readTree(y);
                like(username, hostname, nx, ny);
                return "";
            }
            if (t.equals("undo_like")) {
                if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                        .getScheme()
                        .equals("https")) {
                    res.status(400);
                    return "";
                }
                String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
                if (y == null || y.isEmpty()) {
                    res.status(500);
                    return "";
                }
                JsonNode ny = objectMapper.readTree(y);
                undoLike(username, hostname, nx, ny);
                return "";
            }
            if (t.equals("announce")) {
                if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                        .getScheme()
                        .equals("https")) {
                    res.status(400);
                    return "";
                }
                String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
                if (y == null || y.isEmpty()) {
                    res.status(500);
                    return "";
                }
                JsonNode ny = objectMapper.readTree(y);
                announce(username, hostname, nx, ny);
                return "";
            }
            if (t.equals("undo_announce")) {
                if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                        .getScheme()
                        .equals("https")) {
                    res.status(400);
                    return "";
                }
                String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
                if (y == null || y.isEmpty()) {
                    res.status(500);
                    return "";
                }
                JsonNode ny = objectMapper.readTree(y);
                String z;
                if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                    z = send.get("url").textValue();
                } else {
                    z = "https://" + hostname + "/u/" + username + "/s/00000000000000000000000000000000";
                }
                if (!URI.create(z).getScheme().equals("https")) {
                    res.status(400);
                    return "";
                }
                undoAnnounce(username, hostname, nx, ny, z);
                return "";
            }
            if (t.equals("create_note")) {
                String y;
                if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                    y = send.get("url").textValue();
                } else {
                    y = "https://localhost";
                }
                if (!URI.create(y).getScheme().equals("https")) {
                    res.status(400);
                    return "";
                }
                createNote(username, hostname, nx, y);
                return "";
            }
            if (t.equals("create_note_image")) {
                String y;
                if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                    y = send.get("url").textValue();
                } else {
                    y = "https://" + hostname + "/public/logo.png";
                }
                if (!URI.create(y).getScheme().equals("https") || !URI.create(y).getHost().equals(hostname)) {
                    res.status(400);
                    return "";
                }
                String z = "image/png";
                if (y.endsWith(".jpg") || y.endsWith(".jpeg")) z = "image/jpeg";
                if (y.endsWith(".svg")) z = "image/svg+xml";
                if (y.endsWith(".gif")) z = "image/gif";
                if (y.endsWith(".webp")) z = "image/webp";
                if (y.endsWith(".avif")) z = "image/avif";
                createNoteImage(username, hostname, nx, y, z);
                return "";
            }
            if (t.equals("create_note_mention")) {
                if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                        .getScheme()
                        .equals("https")) {
                    res.status(400);
                    return "";
                }
                String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
                if (y == null || y.isEmpty()) {
                    res.status(500);
                    return "";
                }
                JsonNode ny = objectMapper.readTree(y);
                String z;
                if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                    z = send.get("url").textValue();
                } else {
                    z = "https://localhost";
                }
                if (!URI.create(z).getScheme().equals("https")) {
                    res.status(400);
                    return "";
                }
                createNoteMention(username, hostname, nx, ny, z);
                return "";
            }
            if (t.equals("create_note_hashtag")) {
                String y, z;
                if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                    y = send.get("url").textValue();
                } else {
                    y = "https://localhost";
                }
                if (!URI.create(y).getScheme().equals("https")) {
                    res.status(400);
                    return "";
                }
                if (send.hasNonNull("tag") && !send.path("tag").textValue().isEmpty()) {
                    z = send.get("tag").textValue();
                } else {
                    z = "Hashtag";
                }
                createNoteHashtag(username, hostname, nx, y, z);
                return "";
            }
            if (t.equals("update_note")) {
                String y;
                if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                    y = send.get("url").textValue();
                } else {
                    y = "https://" + hostname + "/u/" + username + "/s/00000000000000000000000000000000";
                }
                if (!URI.create(y).getScheme().equals("https")) {
                    res.status(400);
                    return "";
                }
                updateNote(username, hostname, nx, y);
                return "";
            }
            if (t.equals("delete_tombstone")) {
                String y;
                if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                    y = send.get("url").textValue();
                } else {
                    y = "https://" + hostname + "/u/" + username + "/s/00000000000000000000000000000000";
                }
                if (!URI.create(y).getScheme().equals("https")) {
                    res.status(400);
                    return "";
                }
                deleteTombstone(username, hostname, nx, y);
                return "";
            }
            System.out.println("TYPE " + aid + " " + atype);
            return "";
        });

        get("/.well-known/nodeinfo", (req, res) -> {
            String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();

            Map<String, Object> body = new HashMap<>();
            List<Map<String, String>> linksList = new ArrayList<>();
            body.put("links", linksList);
            linksList.add(new HashMap<>());
            linksList.get(0).put("rel", "http://nodeinfo.diaspora.software/ns/schema/2.0");
            linksList.get(0).put("href", "https://" + hostname + "/nodeinfo/2.0.json");
            linksList.add(new HashMap<>());
            linksList.get(1).put("rel", "http://nodeinfo.diaspora.software/ns/schema/2.1");
            linksList.get(1).put("href", "https://" + hostname + "/nodeinfo/2.1.json");

            Map<String, String> headers = new HashMap<>();
            headers.put("Cache-Control", "public, max-age=" + CONFIG.get("ttl").asText() + ", must-revalidate");
            headers.put("Vary", "Accept, Accept-Encoding");
            headers.forEach(res::header);

            return objectMapper.writeValueAsString(body);
        });

        get("/.well-known/webfinger", (req, res) -> {
            String username = CONFIG.get("actor").get(0).get("preferredUsername").textValue();
            String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
            String p443 = "https://" + hostname + ":443/";
            String resource  = req.queryParams("resource");
            Boolean hasResource = false;
            if (resource.startsWith(p443)) {
                resource = "https://" + hostname + "/" + resource.substring(p443.length());
            }
            if (resource.equals("acct:" + username + "@" + hostname)) hasResource = true;
            if (resource.equals("mailto:" + username + "@" + hostname)) hasResource = true;
            if (resource.equals("https://" + hostname + "/@" + username)) hasResource = true;
            if (resource.equals("https://" + hostname + "/u/" + username)) hasResource = true;
            if (resource.equals("https://" + hostname + "/user/" + username)) hasResource = true;
            if (resource.equals("https://" + hostname + "/users/" + username)) hasResource = true;
            if (!hasResource) return null;

            Map<String, Object> body = new HashMap<>();
            List<String> aliasesList = new ArrayList<>();
            List<Map<String, String>> linksList = new ArrayList<>();
            body.put("subject", "acct:" + username + "@" + hostname);
            body.put("aliases", aliasesList);
            aliasesList.add("mailto:" + username + "@" + hostname);
            aliasesList.add("https://" + hostname + "/@" + username);
            aliasesList.add("https://" + hostname + "/u/" + username);
            aliasesList.add("https://" + hostname + "/user/" + username);
            aliasesList.add("https://" + hostname + "/users/" + username);
            body.put("links", linksList);
            linksList.add(new HashMap<>());
            linksList.get(0).put("rel", "self");
            linksList.get(0).put("type", "application/activity+json");
            linksList.get(0).put("href", "https://" + hostname + "/u/" + username);
            linksList.add(new HashMap<>());
            linksList.get(1).put("rel", "http://webfinger.net/rel/avatar");
            linksList.get(1).put("type", "image/png");
            linksList.get(1).put("href", "https://" + hostname + "/public/" + username + "u.png");
            linksList.add(new HashMap<>());
            linksList.get(2).put("rel", "http://webfinger.net/rel/profile-page");
            linksList.get(2).put("type", "text/plain");
            linksList.get(2).put("href", "https://" + hostname + "/u/" + username);

            Map<String, String> headers = new HashMap<>();
            headers.put("Cache-Control", "public, max-age=" + CONFIG.get("ttl").asText() + ", must-revalidate");
            headers.put("Vary", "Accept, Accept-Encoding");
            headers.forEach(res::header);

            res.type("application/jrd+json");
            return objectMapper.writeValueAsString(body);
        });

        redirect.get("/@", "/");
        redirect.get("/u", "/");
        redirect.get("/user", "/");
        redirect.get("/users", "/");

        get("/users/:username", (req, res) -> {
            res.redirect("/u/" + req.params("username"));
            return "";
        });
        get("/user/:username", (req, res) -> {
            res.redirect("/u/" + req.params("username"));
            return "";
        });
        get("/:strRoot", (req, res) -> {
            if (!req.params(":strRoot").startsWith("@")) return null;
            res.redirect("/u/" + req.params(":strRoot").substring(1));
            return "";
        });
    }
}
