# StrawberryFields Spark

- [2.0.0]
- [1.6.0] - 2024-08-10 - 9 files changed, 159 insertions(+), 105 deletions(-)
- [1.5.0] - 2024-03-10 - 3 files changed, 112 insertions(+), 45 deletions(-)
- [1.4.0] - 2024-02-04 - 6 files changed, 141 insertions(+), 92 deletions(-)
- [1.3.0] - 2023-11-11 - 6 files changed, 33 insertions(+), 23 deletions(-)
- [1.2.0] - 2023-11-05 - 9 files changed, 257 insertions(+), 148 deletions(-)
- [1.1.0] - 2023-08-19 - 2 files changed, 8 insertions(+), 8 deletions(-)
- 1.0.0 - 2023-08-16

[2.0.0]: https://gitlab.com/acefed/strawberryfields-spark/-/compare/7df4548b...main
[1.6.0]: https://gitlab.com/acefed/strawberryfields-spark/-/compare/93c40fda...7df4548b
[1.5.0]: https://gitlab.com/acefed/strawberryfields-spark/-/compare/03c9d631...93c40fda
[1.4.0]: https://gitlab.com/acefed/strawberryfields-spark/-/compare/6d268ee2...03c9d631
[1.3.0]: https://gitlab.com/acefed/strawberryfields-spark/-/compare/e3559661...6d268ee2
[1.2.0]: https://gitlab.com/acefed/strawberryfields-spark/-/compare/a29ea1c9...e3559661
[1.1.0]: https://gitlab.com/acefed/strawberryfields-spark/-/compare/57613197...a29ea1c9
